/**
 * Created by kimduk on 25.04.14.
 */
$(document).ready(function () {
    var conn = new WebSocket('ws://172.16.2.36:8080'),
        isOnMessage = false;

    var jPlayer = new jPlayerPlaylist({
        jPlayer: "#jplayer",
        cssSelectorAncestor: "#jp_container"
    }, [
        {
            title:"Song",
            mp3:"/media/song.mp3"
        },
        {
            title:"Song01",
            mp3:"/media/song01.mp3"
        },
        {
            title:"Song02",
            mp3:"/media/song02.mp3"
        },
        {
            title:"Song03",
            mp3:"/media/song03.mp3"
        },
        {
            title:"Song04",
            mp3:"/media/song04.mp3"
        }
    ], {
        swfPath: "/js",
        supplied: "oga, mp3",
        wmode: "window",
        smoothPlayBar: true,
        keyEnabled: true
    });

    $("#jplayer_inspector_1").jPlayerInspector(
        {
            jPlayer:$("#jplayer")
        }
    );

    $("#jplayer").bind($.jPlayer.event.seeked, function(event) {
        console.log("press SEEKING");
        $("#playBeganAtTime").text("Play began at time = " + event.jPlayer.status.currentTime);
        if (!isOnMessage) {
//            conn.send('{"action": "seeked", "data": {"current":' + jPlayer.current + ', "timeStart": ' + event.jPlayer.status.currentTime + '}}');
        }
        isOnMessage = false;
    });

    $("#jplayer").bind($.jPlayer.event.play, function(event) {
        console.log("press PLAY");
        if (!isOnMessage) {
            conn.send('{"action": "play", "data": {"current":' + jPlayer.current + ', "timeStart": ' + event.jPlayer.status.currentTime + ' }}');
        }
        isOnMessage = false;
    });

    $("#jplayer").bind($.jPlayer.event.pause, function(event) {
        console.log("press PAUSE");
        if (!isOnMessage) {
            conn.send('{"action": "pause", "data": {"current":' + jPlayer.current + ', "timeStart": ' + event.jPlayer.status.currentTime + '}}');
        }
        isOnMessage = false;
    });

    $("#jplayer").bind($.jPlayer.event.volumechange, function(event) {
        if (!isOnMessage) {
            if(event.jPlayer.options.muted == true) {
                var action = "mute";
            } else {
                var action = "unmute";
            }
            conn.send('{"action": "' + action + '", "data": {"current":' + jPlayer.current + ', "timeStart": ' + event.jPlayer.status.currentTime + ' }}');
        }
        isOnMessage = false;
    });

    conn.onopen = function(e) {
        console.log('Connection established!');
    };

    conn.onmessage = function(e) {
        var data = JSON.parse(e.data);
        console.log("STATUS:" + data.status);
        console.log("ACTION:" + data.action);

        jPlayer.current = data.songId;

        if (data.action == "load") {
            if (data.status == "plying") {
                /** значит клиент только что подключился и трек проигрывается */
                isOnMessage = true;
                jPlayer.play(data.songId);
                isOnMessage = true;
                $("#jplayer").jPlayer("play", data.timeStart);
            }
        } else {
            if (data.action == "play") {
                jPlayer.play(data.songId);
            }
            $("#jplayer").jPlayer(data.action, data.timeStart);
            isOnMessage = true;
        }
    };

});