<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 25.04.14
 * Time: 15:02
 */

namespace StreamAudio;

use ___PHPSTORM_HELPERS\object;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\WebSocket\Version\RFC6455\Connection;

class Control implements MessageComponentInterface
{

    protected $_clients,
        $_songId = null,
        $_timeBeginPlay = null,
        $_status = "stopping";

    public function __construct()
    {
        $this->_clients = new \SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->_clients->attach($conn);

        if($this->_songId !== null) {
            $beginFrom = time() - $this->_timeBeginPlay;
            $conn->send(json_encode(["action" => "load", "status" => $this->_status, "songId" => $this->_songId, "timeStart" => $beginFrom ]));
        }

        echo "New connection \n";
    }

    public function onMessage(ConnectionInterface $from, $data)
    {
        $numRecv = count($this->_clients);

        $dataObj = json_decode($data);

        $this->_songId = $dataObj->data->current;
        $this->_timeBeginPlay = time();

        echo $dataObj->action . "\n";
        echo $this->_songId . "\n";
        echo $dataObj->timeStart . "\n";

        if ($dataObj->action === "pause") {
            $this->_status = "stopping";
        } else {
            $this->_status = "plying";
        }

        foreach ($this->_clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send(json_encode(["action" => $dataObj->action, "songId" => $this->_songId, "timeStart" => $dataObj->data->timeStart]));
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->_clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}