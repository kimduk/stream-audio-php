<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 25.04.14
 * Time: 15:15
 */

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use StreamAudio\Control;

require dirname(__DIR__) . "/vendor/autoload.php";

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Control()
        )
    ),
    8080
);

$server->run();